﻿using GTA;
using GTA.Native;
using System;
using System.Windows.Forms;

namespace PoliceBlips
{
    public class PoliceBlips : Script
    {
        Keys toggleKey;
        bool showMessage;

        bool policeBlipsEnabled;

        public PoliceBlips()
        {
            policeBlipsEnabled = Settings.GetValue("SETTINGS", "BLIPS_ENABLED_ON_STARTUP", true);
            toggleKey = Settings.GetValue("SETTINGS", "KEY", Keys.End);
            showMessage = Settings.GetValue("SETTINGS", "SHOW_TOGGLED_MESSAGE", true);

            if (toggleKey != Keys.None) KeyUp += PoliceBlips_KeyUp;
            Interval = 500;
            Tick += PoliceBlips_Tick;
        }

        private void PoliceBlips_Tick(object sender, EventArgs e)
        {
            Function.Call(Hash.SET_POLICE_RADAR_BLIPS, policeBlipsEnabled);
        }

        private void PoliceBlips_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == toggleKey)
            {
                policeBlipsEnabled = !policeBlipsEnabled;

                if (showMessage) UI.ShowSubtitle("Police radar blips " + (policeBlipsEnabled ? "enabled" : "disabled"));
            }
        }
    }
}
